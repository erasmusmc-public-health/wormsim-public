This is the public git repository for the wormsim project. 

## For Downloads of releases:
 
* go to the *tags* link just below the *Wormsim.public* heading on this page
* select a tag/version
* click the download button at the right and select a format.

## For documentation:

Please refer to the wiki, use the *wiki* button in the left menu. 