<?xml version="1.0" encoding="UTF-8"?>
<!-- This file is a subdocument of the wormsim.xsd schema. -->
<xs:schema 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"
    xmlns:inheritance="http://jaxb2-commons.dev.java.net/basic/inheritance"
    jaxb:extensionBindingPrefixes="inheritance"
    elementFormDefault="qualified" 
    jaxb:version="2.1">

    <xs:include schemaLocation="sim-commons/commonTypes.xsd" />
    
<!-- ########################### Test ##############################    -->
<!-- #####################################################################    -->
    <xs:element name="diagnostics.table">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:extends>
                    nl.erasmusmc.mgz.core.input.generation.common.XmlMappableTable&lt;XmlDiagnostics&gt;
                </inheritance:extends>  
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.IXmlMappableTable&lt;XmlDiagnostics&gt;
                </inheritance:implements>  
                <jaxb:class>
                    <jaxb:javadoc>
Test section of the XmlInput, defining the diagnostic tests for the diseases.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element maxOccurs="unbounded" ref="diagnostics" >
                    <xs:annotation>
                        <xs:appinfo>
                            <jaxb:property name="elements"/>
                        </xs:appinfo>
                    </xs:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    
    <xs:element name="diagnostics">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.IXmlNamedEntity
                </inheritance:implements>  
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
Definition of a diagnostic test on a disease. 
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="test.result" type="diagnostics.test.result"/>
            </xs:sequence>
            <xs:attribute name="name" use="required" type="xs:ID">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property>
                            <jaxb:javadoc>
The name under which this test can be referenced. It can be referenced from screening under 
input > control > mass-treatment > screening.table > screening ({@link XmlControlMassTreatementScreening}).
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="disease.ref" use="required" type="xs:IDREF">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property>
                            <jaxb:javadoc>
References a disease, under the input > disease.table > disease element ({@link XmlDisease}). 
Must match the name attribute of a disease element.  
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="type" use="required" >
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property>
                            <jaxb:javadoc>
The causing parasite life phase where this test works on. Must
be one of &lt;code&gt;adult&lt;/code&gt; or &lt;code&gt;offspring&lt;/code&gt;.
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="adult"/>
                        <xs:enumeration value="offspring"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:attribute>
        </xs:complexType>
    </xs:element>

    <xs:complexType name="distribution.multisample.restricted">
        <xs:complexContent>
            <!-- restriction of multisample distro, making mean prohibited -->
            <xs:restriction base="distribution.multisample">
                <xs:attribute name="mean" use="prohibited" type="xs:double" />
            </xs:restriction>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="diagnostics.test.result">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:class>
                    <jaxb:javadoc>
                        Defines the result of this diagnostic test.  
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:complexContent>
            <!-- extension of multisample distro, defining an extra multiplier attribute -->
            <xs:extension base="distribution.multisample.restricted">
                <xs:attribute name="units.of.bio.material" use="required" type="positiveDouble">
                    <xs:annotation>
                        <xs:appinfo>
                            <jaxb:property>
                                <jaxb:javadoc>
This item defines the average expected microfilariae to be found in one skin snip test 
for one fully reproductive adult female worm. It is analogous to disease.parasite.fertility@potential.production, 
but where that one is the basis of the calculation of present microfilariae for transmission, this one is used
solely for the calculation of diagnostic skin snip tests. Usually, the two would be equal, but a set-up with 
two different parameters like this allows us to tune the sensitivity of the tests without affecting the actual 
transmission. 
                                </jaxb:javadoc>
                            </jaxb:property>
                        </xs:appinfo>
                    </xs:annotation>
                </xs:attribute>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    

</xs:schema>
