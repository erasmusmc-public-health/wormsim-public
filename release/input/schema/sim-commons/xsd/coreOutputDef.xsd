<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    This file contains the xsd definition for the output of sim-commons. 
    Sim-commons only defines general elements relevant for output: basically 
    just times and age classes. 
-->
<xs:schema 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"
    xmlns:inheritance="http://jaxb2-commons.dev.java.net/basic/inheritance"
    jaxb:extensionBindingPrefixes="inheritance" 
    elementFormDefault="qualified" 
    jaxb:version="2.1">

    <xs:include schemaLocation="commonTypes.xsd" />

<!--
    <xs:annotation>
        <xs:appinfo> 
            <jaxb:schemaBindings>
                <jaxb:package name="nl.erasmusmc.mgz.core.input.generated.output"/>
                <jaxb:nameXmlTransform>  
                    <jaxb:typeName prefix="XmlCore"/>
                    <jaxb:elementName prefix="XmlCore"/>
                    <jaxb:modelGroupName prefix="XmlCore"/>
                    <jaxb:anonymousTypeName prefix="XmlCore"/> 
                </jaxb:nameXmlTransform>
            </jaxb:schemaBindings>
        </xs:appinfo>
    </xs:annotation> 
    -->
    
    <!-- ########################### Main Entry ##############################    -->

    <xs:complexType name="core.output">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.output.generation.IXmlCoreOutput
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
Defines the form and content of the output. This root class will contain all elements which
are specified in the outputDef.xml file.
&lt;p&gt;In the outputDef.xml file, this tag is the root tag of the xml document. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output &lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xmlns:inheritance="http://jaxb2-commons.dev.java.net/basic/inheritance"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xsi:noNamespaceSchemaLocation="../../../main/resources/outputDef.xsd"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other elements go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="periodic" minOccurs="0" type="core.output.periodic">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property>
                            <jaxb:javadoc>
Defines periodical output, so output on regular intervals with a start and end date.
See javadoc on the type for more explanation. If it is not specified, there will be no output.
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
            <xs:element name="additional" minOccurs="0" type="core.output.additional.moment.table">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property>
                            <jaxb:javadoc>
This class allows you to define a list of additional output moments. At these points in 
time an additional line of output is generated. This property and the interval property 
work independent of each other (in the sense that none, both, or one of them can be specified). 
All output goes to the same output file. See jovadoc on the type for more explanation.
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
            <xs:element name="age.table" type="core.output.age.table"/>
        </xs:sequence>
        <xs:attribute name="sex" use="optional" default="false" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
The sex attribute tells output to distinguish between sexes in output. It adds a sex column to output if true.
If the client implementation doesn't distinguish between sexes, you may just ignore this attribute. As
it defaults to false, end users don't have to specify it. 
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <!-- ########################### Times ##############################    -->
    <!-- -  -  -  -  -  -  -  -  - Periodic -  -  -  -  -  -  -  -  -   -->
    
    <xs:complexType name="core.output.periodic">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.output.generation.IXmlCoreOutputPeriodic
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
Defines the output at regular intervals, with a preset start and end date. 
&lt;p&gt;In the outputDef.xml file, this tag is placed inside the root output-tag. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;periodic&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;start year="2000" month="jan" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;stop year="2020" month="feb" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;interval&amp;gt;1.0&amp;lt;/interval&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;time.window&amp;gt;1.0&amp;lt;/time.window&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/periodic&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other elements go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="start" use="optional" type="dateTime">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
Output starts with this point in time. If null, the start of the simulation is to be used.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="stop" use="required" type="dateTime">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
Output stops at this point in time. Obligatory
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="interval" use="optional" type="duration">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
For each interval passed a line will be written to the output. If null or 0, no periodic output
will be generated. However, additional output moments can still be defined in that case. 
See the javadoc of the {@link IXmlDuration} for allowed formats. 
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="time.window" use="optional" type="duration">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
At each interval, the results over this timeWindow will be written to output. If null, the 
timeWindow is the same as the interval. 
See the javadoc of the {@link IXmlDuration} for allowed formats. 
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attributeGroup ref="delay"/>
    </xs:complexType>

    <!-- -  -  -  -  -  -  -  -  - Additional moments -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="core.output.additional.moment.table">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.output.generation.IXmlCoreOutputAdditionalMomentTable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
Allows the user to set a number of custom points in time at which an output line is generated. 
&lt;p&gt;In the outputDef.xml file, this tag is placed inside the root output-tag. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;additional&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;moment year="2010" month="jul" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;moment year="2011" month="sep" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/additional&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other elements go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element maxOccurs="unbounded" name="moment" type="core.output.additional.moment">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="moments"/>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    
    <xs:complexType name="core.output.additional.moment">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.output.generation.IXmlCoreOutputAdditionalMoment
                </inheritance:implements>  
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
A moment in time (timepoint) outside the pattern/schema of periodic output, at which output is generated.                          
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="timepoint" use="required" type="dateTime"/>
        <xs:attribute name="repeat" use="optional" type="positiveInt" default="1"/>
    </xs:complexType>
    
    <!-- -  -  -  -  -  -  -  -  - Age table -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="core.output.age.table">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:extends>
                    nl.erasmusmc.mgz.core.input.generation.common.XmlAgeIndex
                </inheritance:extends>  
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.IXmlAgeIndex
                </inheritance:implements>  
                <jaxb:class>
                    <jaxb:javadoc>
Defines age classes for the output. These age classes are only valid for output; for
other features, other age class definitions may be in use. 
&lt;p&gt;In the outputDef.xml file, this tag is placed inside the root output-tag. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;age.table&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;age upper.age.limit="5" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;age upper.age.limit="20" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;age upper.age.limit="55" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;age upper.age.limit="95" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/age.table&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other elements go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element maxOccurs="unbounded" name="age" type="core.output.age">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="entries"/>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="core.output.age">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.IXmlAgeClass
                </inheritance:implements>  
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
Defines a single age class for the output. The age class is limited by the specified 
upper age limit, and the lower age limit is the upper age limit of the previous age class
(or 0 if no previous age class was present). As age limit, any integer between 0 (inclusive)
and 200 (exclusive) may be used. 
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="upper.age.limit" use="required" type="age"/>
    </xs:complexType>

</xs:schema>
