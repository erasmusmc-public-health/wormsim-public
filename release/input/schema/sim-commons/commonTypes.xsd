<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Common datatype definitions valid for the sim-commons core project, version 1.0.
-->
<!--
    The default namespace is needed because the common types need their own namespace, otherwise
        they can't be generated in their own package. If they're not generated in their own
        packages they will be generated twice (once for input, once for output).
    The target namespace is needed because otherwise the import statement from the xsd's 
        using this one won't work.
    The vc namespace is just needed for the vc:minVersion; needed for versioning to allow asserts. 
    The jaxb namespace is needed to allow javadoc and global prefixing with Xml of classes.
    The inheritance namespace/plugin allows to let generated classes to extend or implement something,
        this is used for the common time related types.
    The jaxb:extensionBindingPrefixes attribute is needed to allow xjc to use the inheritance plugin.
-->
<xs:schema
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"
    xmlns:inheritance="http://jaxb2-commons.dev.java.net/basic/inheritance"
    jaxb:extensionBindingPrefixes="inheritance"
    elementFormDefault="qualified" 
    jaxb:version="2.1">

<!--
    <xs:annotation>
        <xs:appinfo> 
            <jaxb:schemaBindings>
                <jaxb:package name="nl.erasmusmc.mgz.generated.common"/>
                <jaxb:nameXmlTransform>  
                    <jaxb:typeName prefix="Xml"/>
                    <jaxb:elementName prefix="Xml"/>
                    <jaxb:modelGroupName prefix="Xml"/>
                    <jaxb:anonymousTypeName prefix="Xml"/> 
                </jaxb:nameXmlTransform>
            </jaxb:schemaBindings>
        </xs:appinfo>
    </xs:annotation> 
    -->
    <xs:complexType name="common.simulation">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.IXmlCommonSimulation
                </inheritance:implements>  
                <jaxb:class>
                    <jaxb:javadoc>
                        Simulation section of the XmlInput, containing times for the simulation. It contains
                        a start and stoptime, and an interval at which a sign of life is to be reported. 
                        Note that this complextype may be extended in client xsd's, if more attributes 
                        or child elements are needed. 
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="start" use="required" type="dateTime"/>
        <xs:attribute name="stop" type="dateTime"/>
        <xs:attribute name="progressIndicatorInterval" type="duration"/>
    </xs:complexType>

    <xs:simpleType name="dateTime">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="nl.erasmusmc.mgz.core.input.generation.common.IXmlTimePoint"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDateTime"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalDateTime"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="\d{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])(T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9](\.\d{1,3})?))?"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="month">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="java.time.Month"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalMonth"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalMonth"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:token">
            <xs:enumeration value="jan"/>
            <xs:enumeration value="feb"/>
            <xs:enumeration value="mar"/>
            <xs:enumeration value="apr"/>
            <xs:enumeration value="may"/>
            <xs:enumeration value="jun"/>
            <xs:enumeration value="jul"/>
            <xs:enumeration value="aug"/>
            <xs:enumeration value="sep"/>
            <xs:enumeration value="oct"/>
            <xs:enumeration value="nov"/>
            <xs:enumeration value="dec"/>
            <xs:enumeration value="january"/>
            <xs:enumeration value="february"/>
            <xs:enumeration value="march"/>
            <xs:enumeration value="april"/>
            <xs:enumeration value="may"/>
            <xs:enumeration value="june"/>
            <xs:enumeration value="july"/>
            <xs:enumeration value="august"/>
            <xs:enumeration value="september"/>
            <xs:enumeration value="october"/>
            <xs:enumeration value="november"/>
            <xs:enumeration value="december"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="duration">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="nl.erasmusmc.mgz.core.input.generation.common.IXmlDuration"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDuration"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalDuration"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="\d*\.?\d+[yYmMwWdDhHsS]?"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="duration.allow.neg">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="nl.erasmusmc.mgz.core.input.generation.common.IXmlDuration"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDurationAllowNeg"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalDurationAllowNeg"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="-?\d*\.?\d+[yYmMwWdDhHsS]?"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:attributeGroup name="delay">
        <!--            
            Delay is an offset given to the declared time, meaning that the event will be executed at 
            declared time + delay. Note that delay is a duration, so its syntax follows the normal duration
            syntax, except that a delay may be negative. Leaving delay out means that it will be converted
            to null; client applications must explicitly null-check delays before using them. 
         -->
        <xs:attribute name="delay" type="duration.allow.neg" use="optional" />
    </xs:attributeGroup>
    
    <xs:simpleType name="age">
        <!-- Age is a special case of duration, allowing only years -->
        <xs:annotation>
            <xs:appinfo>
                <!-- Takes care of converting to our own custom duration type. -->
                <jaxb:javaType 
                    name="nl.erasmusmc.mgz.core.input.generation.common.IXmlDuration"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalAge"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalDuration"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:double">
            <xs:minInclusive value="0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:attributeGroup name="ageSexTableEntryWithFractions">
        <xs:attribute name="upper.age.limit" use="required" type="age"/>
        <xs:attribute name="female" type="fractionNumber"/>
        <xs:attribute name="male" type="fractionNumber"/>
        <xs:attribute name="both" type="fractionNumber"/>
    </xs:attributeGroup>
    
    
    <!-- 
        use this attribute group for a table which supports linear interpolation and 
        stepwise interpretation, which applies stepwise by default.
        Checkout the IXmlInterpolatableTable javadoc for details.
    --> 
    <xs:attributeGroup name="interpolateAttrDefaultFalse">
        <xs:attribute name="interpolate" use="optional" default="false" type="xs:boolean"/>
    </xs:attributeGroup>
    
    <!-- use this attribute group for a table which supports linear interpolation and 
        stepwise interpretation, which applies linear by default --> 
    <xs:attributeGroup name="interpolateAttrDefaultTrue">
        <xs:attribute name="interpolate" use="optional" default="true" type="xs:boolean"/>
    </xs:attributeGroup>
    
    <xs:attributeGroup name="sexAttr">
        <xs:attribute name="sex" use="optional" default="both" type="sex">
<!--            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
Specifies the sex to which this element refers. &lt;p&gt;
It can have the following values in the input file:
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;male:&lt;/b&gt;
in this case the parent in which this is used must occur twice, 
once for each sex. Will be converted to javatype &lt;code&gt;Sex.MALE&lt;/code&gt;.
&lt;li&gt;&lt;b&gt;female:&lt;/b&gt;
analogous, will be converted to javatype &lt;code&gt;Sex.FEMALE&lt;/code&gt;.
&lt;li&gt;&lt;b&gt;both:&lt;/b&gt;
default value. the parent in which this is used may occur only one time.
Will be converted to a javatype &lt;code&gt;Sex&lt;/code&gt; with value 
&lt;code&gt;NULL&lt;/code&gt;.
&lt;/ul&gt;
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>  -->  
        </xs:attribute>
    </xs:attributeGroup>  

    <xs:simpleType name="sex">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                   name="nl.erasmusmc.mgz.core.demography.agent.Sex"
                   parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalSex"
                   printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalSex"
                 />  
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:token">
            <xs:enumeration value="male"/>
            <xs:enumeration value="female"/>
            <xs:enumeration value="both"/>
        </xs:restriction>
    </xs:simpleType>
    
    
    <!-- ######################################################################################### -->
    <!-- ############                       NUMBERICAL TYPES                ###################### -->
    <!-- ######################################################################################### -->
    
    <!-- 
        In general, numerical xsd types will convert to primitives. For the age/sex tables and other 
        tables which take either doubles or integers, we need conversion to types implementing java.lang.Number, 
        like Double or Integer. 
        In such cases the xsd type is suffixed with "Number", and conversion is done via XmlTypeConverter.
        
        In general, all primitive-backed xsd types are present. Only few *Number types are available. 
        If you need more Number backed xsd types, please add them as needed. 
    -->
    
    
    <xs:simpleType name="fraction">
        <xs:restriction base="xs:double">
            <xs:minInclusive value="0.0"/>
            <xs:maxInclusive value="1.0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="fractionNumber">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="java.lang.Double"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDouble"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalNumber"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:double">
            <xs:minInclusive value="0.0"/>
            <xs:maxInclusive value="1.0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="fractionNot1">
        <xs:restriction base="xs:double">
            <xs:minInclusive value="0.0"/>
            <xs:maxExclusive value="1.0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="nonnegdouble">
        <xs:restriction base="xs:double">
            <xs:minInclusive value="0.0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="nonnegdoubleNumber">
        <!-- The difference between *double and *doubleNumber is that the first translates to a primitive
        double, and the second to java.lang.Double. -->
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="java.lang.Double"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDouble"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalNumber"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:double">
            <xs:minInclusive value="0.0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="positiveDoubleNumber">
        <!-- The difference between *double and *doubleNumber is that the first translates to a primitive
        double, and the second to java.lang.Double. -->
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="java.lang.Double"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDouble"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalNumber"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:double">
            <xs:minExclusive value="0.0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="nonnegint">
        <xs:restriction base="xs:int">
            <xs:minInclusive value="0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="nonnegintNumber">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="java.lang.Integer"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalInt"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalNumber"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:int">
            <xs:minInclusive value="0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="positiveDouble" id="positiveDouble">
        <xs:restriction base="xs:double">
            <xs:minExclusive value="0"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="positiveInt" id="positiveInt">
        <xs:restriction base="xs:int">
            <xs:minInclusive value="1"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="doubleArray">
        <!-- an array, space separated, with any double numbers -->
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="double[]"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDoubleArray"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalDoubleArray"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="(-?\d*(\.\d+)?)(\s(-?\d*(\.\d+)?))*"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="doubleNonNegativeArray">
        <!-- an array, space separated, with only non negative double numbers -->
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="double[]"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDoubleArray"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalDoubleArray"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="(\d*(\.\d+)?)(\s(\d*(\.\d+)?))*"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="fractionArray">
        <!-- an array, space separated, with only fractions -->
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="double[]"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalFractionArray"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalFractionArray"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <!-- this pattern allows 1.289302 but at least it does something. The parseMethod will do the real validation. -->
            <xs:pattern value="((0|1)?(\.\d+)?)(\s((0|1)?(\.\d+)?))*"/>
        </xs:restriction>
    </xs:simpleType>
    
    
        
    <!-- ######################################################################################### -->
    <!-- ############                       DISTRIBUTIONS                 ###################### -->
    <!-- ######################################################################################### -->
    
    <xs:attributeGroup name="distributionParamsAtt">
        <xs:attribute name="mean" use="optional" type="xs:double" />
        <xs:attribute name="param" use="optional" type="xs:double" />
        <xs:attribute name="min" use="optional" type="xs:double" />
        <xs:attribute name="max" use="optional" type="xs:double" />
    </xs:attributeGroup>
    
    <!-- attribute group for min and max as durations -->
    <xs:attributeGroup name="distributionParamsDurationAtt">
        <xs:attribute name="mean" use="optional" type="duration" />
        <xs:attribute name="param" use="optional" type="xs:double" />
        <xs:attribute name="min" use="optional" type="duration" />
        <xs:attribute name="max" use="optional" type="duration" />
    </xs:attributeGroup>
    
    <xs:complexType name="distribution.number">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.IXmlContinuousDistribution
                </inheritance:implements>  
                <inheritance:extends>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.XmlAbstractNumberDistribution
                </inheritance:extends>  
                <jaxb:class name="XmlCommonDistributionNumber"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="dist" use="optional" default="con" type="distribution.names"/>
        <xs:attributeGroup ref="distributionParamsAtt"/>
        <xs:attribute name="value" use="optional" type="xs:double"/>
        <xs:attribute name="probabilities" use="optional" type="doubleNonNegativeArray"/>
    </xs:complexType>
    
    <xs:complexType name="distribution.continuous">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.IXmlContinuousDistribution
                </inheritance:implements>  
                <inheritance:extends>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.XmlAbstractContinuousDistribution
                </inheritance:extends>  
                <jaxb:class name="XmlCommonDistributionContinuous"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="dist" use="optional" default="con" type="distribution.continuous.names"/>
        <xs:attributeGroup ref="distributionParamsAtt"/>
        <xs:attribute name="value" use="optional" type="xs:double"/>
    </xs:complexType>
    
    <xs:complexType name="distribution.discrete">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.IXmlDiscreteDistribution
                </inheritance:implements>  
                <inheritance:extends>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.XmlAbstractDiscreteDistribution
                </inheritance:extends>  
                <jaxb:class name="XmlCommonDistributionDiscrete"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="dist" use="required" type="distribution.discrete.names"/>
        <xs:attributeGroup ref="distributionParamsAtt"/>
        <xs:attribute name="probabilities" use="optional" type="doubleNonNegativeArray"/>
    </xs:complexType>
    
    <xs:complexType name="distribution.multisample">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.IXmlMultiSampleDistribution
                </inheritance:implements>  
                <inheritance:extends>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.XmlAbstractMultiSampleDistribution
                </inheritance:extends>  
                <jaxb:class name="XmlCommonDistributionMultisample">
                    <jaxb:javadoc>
                        Class for the multi-sample distribution. It encapsulates the parsed math distributions, and as it 
                        implements IXmlMultiSampleDistribution, you can use it directly via its 
                        &lt;code&gt;multiSample(IMathFactory)&lt;/code&gt; method. &lt;p&gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="dist" use="required" type="distribution.names"/>
        <xs:attribute name="samples" use="required" type="positiveInt"/>
        <xs:attributeGroup ref="distributionParamsAtt"/>
        <xs:attribute name="probabilities" use="optional" type="doubleNonNegativeArray"/>
    </xs:complexType>
    
    <xs:complexType name="distribution.duration">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.IXmlDurationDistribution
                </inheritance:implements>
                <inheritance:extends>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.XmlAbstractDurationDistribution
                </inheritance:extends>  
                <jaxb:class name="XmlCommonDistributionDuration">
                    <jaxb:javadoc>
                        Container class for duration distributions.  
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="dist" use="optional" default="constant" type="distribution.continuous.names"/>
        <xs:attributeGroup ref="distributionParamsDurationAtt"/>
        <!-- in case of the constant distribution...        -->
        <xs:attribute name="value" use="optional" type="duration" />
    </xs:complexType>
    
    <xs:complexType name="distribution.continuous.with.fraction">
        <!--  definition via xs:restriction of a normal continuous distribution did not work because 
      xsd declared defaults for min and max are then just ignored.
      The above does not work with defaults either, because using a default on an attribute
      enforces it to use the primitive double type, and then it doesn't satisfy the
      IXmlContinuousDistribution interface. 
      So we solve this not via xsd but programmatically, by defining an extra abstract parent 
      class for distributions with fractions, who take care of setting the defaults for min and max.
      This saves the end user from always having to specify min and max with distributions 
      on fractions.
-->
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.IXmlContinuousDistribution
                </inheritance:implements>  
                <inheritance:extends>
                    nl.erasmusmc.mgz.core.input.generation.common.distribution.XmlAbstractContinuousDistributionWithFraction
                </inheritance:extends>  
                <jaxb:class name="XmlCommonDistributionContinuousWithFraction">
                    <jaxb:javadoc>
Class for continuous distributions for fractions. Not only the mean, min, max or value attribute
must be a fraction, but the class also forces the result of a call to nextDouble() to a fraction. 
In this terminology, a fraction is defined as a broken number between 0 and 1 (inclusive). 
The nextDouble() result is forced into a fraction by setting the min parameter programmatically to 0, 
and the max parameter programmatically to 1. 
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="dist" use="optional" default="con" type="distribution.continuous.names"/>
        <xs:attribute name="mean" use="optional" type="fraction" />
        <xs:attribute name="param" use="optional" type="xs:double" />
        <xs:attribute name="min" use="optional" type="fraction" />
        <xs:attribute name="max" use="optional" type="fraction" />
        <xs:attribute name="value" use="optional" type="fraction"/>
    </xs:complexType>
    
    <xs:simpleType name="distribution.names">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="nl.erasmusmc.mgz.math.DistributionType"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDistributionType"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalDistributionType"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:union memberTypes="distribution.continuous.names distribution.discrete.names"/>
    </xs:simpleType>
    
    <xs:simpleType name="distribution.continuous.names">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="nl.erasmusmc.mgz.math.DistributionType"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDistributionType"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalDistributionType"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:token">
            <xs:enumeration value="erl"/>
            <xs:enumeration value="exp"/>
            <xs:enumeration value="uni"/>
            <xs:enumeration value="con"/>
            <xs:enumeration value="wei"/>
            <xs:enumeration value="gam"/>
            <xs:enumeration value="nor"/>
            <xs:enumeration value="bet"/>
            <xs:enumeration value="log"/>
            <xs:enumeration value="erlang"/>
            <xs:enumeration value="exponential"/>
            <xs:enumeration value="uniform"/>
            <xs:enumeration value="constant"/>
            <xs:enumeration value="weibull"/>
            <xs:enumeration value="gamma"/>
            <xs:enumeration value="normal"/>
            <xs:enumeration value="beta"/>
            <xs:enumeration value="lognormal"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="distribution.discrete.names">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="nl.erasmusmc.mgz.math.DistributionType"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalDistributionType"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalDistributionType"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:token">
            <xs:enumeration value="ber"/>
            <xs:enumeration value="bin"/>
            <xs:enumeration value="geo"/>
            <xs:enumeration value="neg"/>
            <xs:enumeration value="poi"/>
            <xs:enumeration value="cus"/>
            <xs:enumeration value="bernoulli"/>
            <xs:enumeration value="binomial"/>
            <xs:enumeration value="geometric"/>
            <xs:enumeration value="negativebinomial"/>
            <xs:enumeration value="poisson"/>
            <xs:enumeration value="custom"/>
        </xs:restriction>
    </xs:simpleType>
    
    <!-- ######################################################################################### -->
    <!-- ############                       FUNCTIONS                ###################### -->
    <!-- ######################################################################################### -->
    
    <xs:complexType name="function">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:extends>
                    nl.erasmusmc.mgz.core.input.generation.common.XmlAbstractFunction
                </inheritance:extends>  
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.input.generation.common.IXmlFunction
                </inheritance:implements>  
                <jaxb:class name="XmlCommonFunction">
                    <jaxb:javadoc>
Class for the use of functions; uses math.IFunction internally.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="param" maxOccurs="unbounded" type="distribution.number">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="params">
                            <jaxb:javadoc>
Parameters of the function.  
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="func" use="required" type="function.names"/>
        <xs:attribute name="min" use="optional" type="xs:double" />
        <xs:attribute name="max" use="optional" type="xs:double" />
    </xs:complexType>
    
    <xs:simpleType name="function.names">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:javaType 
                    name="nl.erasmusmc.mgz.math.FunctionType"
                    parseMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.unmarshalFunctionType"
                    printMethod="nl.erasmusmc.mgz.core.input.generation.helper.impl.XmlTypeConverter.marshalFunctionType"
                />
            </xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:token">
            <xs:enumeration value="con"/>
            <xs:enumeration value="lin"/>
            <xs:enumeration value="hyp"/>
            <xs:enumeration value="althyp"/>
            <xs:enumeration value="amm"/>
            <xs:enumeration value="exp"/>
            <xs:enumeration value="sig"/>
            <xs:enumeration value="pow"/>
            <xs:enumeration value="constant"/>
            <xs:enumeration value="linear"/>
            <xs:enumeration value="hyperbolic"/>
            <xs:enumeration value="alternativehyperbolic"/>
            <xs:enumeration value="adaptedmichaelismenten"/>
            <xs:enumeration value="exponential"/>
            <xs:enumeration value="sigmoid"/>
            <xs:enumeration value="power"/>
        </xs:restriction>
    </xs:simpleType>
    
</xs:schema>
