<?xml version="1.0" encoding="UTF-8"?>
<!-- This file contains xsd definitions of output elements which might be used in future. -->

            <xs:element minOccurs="0" name="demography" type="output.element.demography"/>
            <xs:element minOccurs="0" name="survey" type="output.element.survey"/>
            <xs:element minOccurs="0" name="treatment" type="output.element.treatment"/>
            <xs:element minOccurs="0" name="transmission" type="output.element.transmission"/>

    <!-- -  -  -  -  -  -  -  -  - Demography -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.demography">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        When this element is set, it means that demography output is generated. Attributes and child
                        elements may be used to configure the output on demography. You can specify a specific age
                        table for demography, or use the general age table. (/output/age.table).
                        &lt;p&gt;
                        Demography can score the following:
                        &lt;ul&gt;
                        &lt;li&gt;number of people
                        &lt;li&gt;offspring scores
                        &lt;li&gt;number of people with certain types of worms
                        &lt;li&gt;number of people with a certain clinical status
                        &lt;/ul&gt;
                        &lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="age.table" minOccurs="0" type="output.age.table">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property>
                            <jaxb:javadoc>
                                When an age table is set here, it will override the general age table. If you don't want
                                multiple age categories but specified a general one, specify an age table here with just
                                one age class.
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
            <xs:element name="n.persons" minOccurs="0" type="output.demography.n.persons"/>
            <xs:element name="clinical.status" minOccurs="0" type="output.demography.clinical.status"/>
            <xs:element name="worms" minOccurs="0" type="output.demography.worms"/>
            <xs:element name="offspring" minOccurs="0" type="output.demography.offspring"/>
        </xs:sequence>
        <xs:attribute name="sexes" default="false" type="xs:boolean">
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="output.demography.n.persons">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        When this element is set, it means that demography output on general number of persons is
                        generated.
                        You can specify a specific age table here, or else the age table of a parent element will be
                        used.
                        &lt;p&gt;
                        It scores the following:
                        &lt;ul&gt;
                        &lt;li&gt;number of people
                        &lt;li&gt;if sexes="true" the number of males and number of females.
                        &lt;li&gt;number of people the defined age categories (male and female numbers if sexes="true",
                        otherwise
                        just one total score per age category)
                        &lt;/ul&gt;
                        &lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="age.table" minOccurs="0" type="output.age.table">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property>
                            <jaxb:javadoc>
                                When an age table is set here, it will override any parent age table. If you don't want
                                multiple age categories but specified a general one, specify an age table here with just
                                one age class.
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="sexes" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
                            When this attribute is true, output is printed on male and female humans. The default value
                            is the value of the same attribute on the parent element.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="output.demography.clinical.status">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        When this element is set, it means that demography output on the number of persons with a
                        specific clinical status is generated.
                        You can specify a specific age table here, or else the age table of a parent element will be
                        used.
                        &lt;p&gt;
                        It scores the following per given clinical status:
                        &lt;ul&gt;
                        &lt;li&gt;number of people with the clinical status
                        &lt;li&gt;if sexes="true" the number of males and number of females with the clinical status.
                        &lt;li&gt;number of people in the defined age categories with the clinical status (male and
                        female
                        numbers if sexes="true", otherwise just one total score per age category)
                        &lt;/ul&gt;
                        &lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="age.table" minOccurs="0" type="output.age.table">
                <xs:annotation>
                    <xs:appinfo>
                        <inheritance:implements>
                            java.io.Serializable
                        </inheritance:implements>
                        <jaxb:property>
                            <jaxb:javadoc>
                                When an age table is set here, it will override any parent age table. If you don't want
                                multiple age categories but specified a general one, specify an age table here with just
                                one age class.
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="refs" use="required" type="xs:IDREFS">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
                            A list of clinical status id's must be given here. The id's refer to the
                            clinical.status table in the normal input file.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="sexes" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
                            When this attribute is true, output is printed on male and female humans. The default value
                            is the value of the same attribute on the parent element.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="output.demography.worms">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        When this element is set, it means that demography output on worms is generated.
                        You can specify a specific age table here, or else the age table of a parent element will be
                        used.
                        &lt;p&gt;
                        It scores the following:
                        &lt;ul&gt;
                        &lt;li&gt;number of people with the specified type of worms
                        &lt;li&gt;arithmic mean of the number of that type of worm per person.
                        &lt;li&gt;if sexes="true" the above also for male and female humans
                        &lt;li&gt;the first two items for all defined age categories (male and
                        female numbers if sexes="true", otherwise just one total score per age category)
                        &lt;/ul&gt;
                        &lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag.
                        // TODO RINKE #152749391 ADJUST EXAMPLE
                        &lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
                        &amp;lt;output ... &amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;demography sexes="true"&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/prevalence&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        ... other items go here...&lt;br&gt;
                        &amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="age.table" minOccurs="0" type="output.age.table">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property>
                            <jaxb:javadoc>
                                When an age table is set here, it will override any parent age table. If you don't want
                                multiple age categories but specified a general one, specify an age table here with just
                                one age class.
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="sexes" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
                            When this attribute is true, output is printed on male and female humans. The default value
                            is the value of the same attribute on the parent element.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="wormTypes" use="required" type="output.wormTypes">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
                            In this attribute the user can specify a list of worm types on which this output
                            has to be scored. The following types are possible:
                            &lt;ul&gt;
                            &lt;li&gt;&lt;b&gt;prepatent:&lt;/b&gt; prepatent worms of any sex.
                            &lt;li&gt;&lt;b&gt;patent:&lt;/b&gt; patent worms of any sex.
                            &lt;li&gt;&lt;b&gt;patentFemale:&lt;/b&gt; female patent worms.
                            &lt;li&gt;&lt;b&gt;patentPair:&lt;/b&gt; at least one patent male and one patent female worm
                            present.
                            &lt;li&gt;&lt;b&gt;patentFemaleOffspring:&lt;/b&gt; female patent worms reproducing, so with
                            also offspring present.
                            &lt;/ul&gt;
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <xs:simpleType name="output.wormTypes">
        <xs:list>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="prepatent"/>
                    <xs:enumeration value="patent"/>
                    <xs:enumeration value="patentFemale"/>
                    <xs:enumeration value="patentPair"/>
                    <xs:enumeration value="patentFemaleOffspring"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:list>
    </xs:simpleType>

    <xs:complexType name="output.demography.offspring">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        When this element is set, it means that demography output on worm offspring in people is
                        generated.
                        You can specify a specific age table here, or else the age table of a parent element will be
                        used.
                        &lt;p&gt;
                        It scores the following:
                        &lt;ul&gt;
                        &lt;li&gt;arithmetic mean of offspring numbers per person
                        &lt;li&gt;geometric mean of offspring numbers per person
                        &lt;li&gt;if sexes="true" the arithmetic and geometric mean for males and females
                        &lt;li&gt;arithmetic and geometric mean of offspring scores per defined age category
                        (male and female numbers if sexes="true", otherwise just one total score per age category)
                        &lt;li&gt;if an offspring score category table is defined, the number of people in each of these
                        score categories. This is the total number if sexes="false", otherwise it is given for males and
                        for females. Age tables do not apply to this.
                        //TODO RINKE #152846606-4: check the last sentence with Luc or Roel.
                        &lt;/ul&gt;
                        &lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag.
                        // TODO RINKE #152749391 ADJUST EXAMPLE
                        &lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
                        &amp;lt;output ... &amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;demography sexes="true"&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/prevalence&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        ... other items go here...&lt;br&gt;
                        &amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="age.table" minOccurs="0" type="output.age.table">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property>
                            <jaxb:javadoc>
                                When an age table is set here, it will override any parent age table. If you don't want
                                multiple age categories but specified a general one, specify an age table here with just
                                one age class.
                            </jaxb:javadoc>
                        </jaxb:property>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
            <xs:element name="offspring.score.category.table" minOccurs="0"
                        type="output.offspring.score.category.table"/>
        </xs:sequence>
        <xs:attribute name="sexes" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
                            When this attribute is true, output is printed on male and female humans. The default value
                            is the value of the same attribute on the parent element.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>


    <!-- -  -  -  -  -  -  -  -  - Survey -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.survey">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        When this element is set, it means that survey output is generated. Attributes and child
                        elements may be used to configure the output on incidence.
                        You can specify a specific age table for demography, or use the general age table
                        (/output/age.table).
                        // TODO RINKE #152749391 ADJUST EXAMPLE
                        &lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag.
                        &lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
                        &amp;lt;output ... &amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;incidence personYears="true"&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;state name="susceptible" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;state name="alive" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/incidence&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        ... other items go here...&lt;br&gt;
                        &amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="age.table" minOccurs="0" type="output.age.table"/>
            <xs:element name="survey.table" type="output.survey.table"/>
            <xs:element name="offspring.score.category.table" minOccurs="0"
                        type="output.offspring.score.category.table"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="output.survey.table">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        Defines surveys (clinical tests) which are used to print output on.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element maxOccurs="unbounded" name="survey" type="output.survey">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="surveys"/>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="output.survey">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        Defines a single survey (clinical test) which is used to generate output on. The survey's
                        linked participation is applied before generating the output.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="ref" use="required" type="xs:string"/>
    </xs:complexType>

    <!-- -  -  -  -  -  -  -  -  - treatment -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.treatment">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        When this element is set, it means that treatment output is generated. Attributes and child
                        elements may be used to configure the output on vector. &lt;br&gt;
                        Treatment is different, in this respect that it ignores any given times in outputDef. This is
                        because it only generates output at every treatment round.
                        &lt;p&gt;
                        It scores the following:
                        &lt;ul&gt;
                        &lt;li&gt;Number of participants; if sexes="true" for male and for female, otherwise for all
                        together.
                        &lt;li&gt;Number of non-participants, analogous
                        &lt;li&gt;Both previous item for all defined age classes.
                        &lt;li&gt;if distrTable="true", scores a frequency distribution table with the number of people
                        participating in 0, 1,.. n treatment rounds. Sexes and age tables are ignored in this option.
                        //TODO RINKE #152846606-4: check the last sentence with Luc or Wilma.
                        &lt;/ul&gt;
                        //TODO RINKE #152749391 ADAPT EXAMPLE.
                        &lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag.
                        &lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
                        &amp;lt;output ... &amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;vector vectorBites="true" toVectorFOI="true"&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;state name="susceptible" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;state name="alive" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/vector&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        ... other items go here...&lt;br&gt;
                        &amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="age.table" minOccurs="0" type="output.age.table"/>
        </xs:sequence>
        <xs:attribute name="sexes" default="false" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
                            When this attribute is true, output is printed on male and female humans. The default value
                            is false, in which case no division for males and females are printed.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="distrTable" default="false" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
                            If true, scores a frequency distribution table with the number of people
                            participating in 0, 1,.. n treatment rounds. Sexes and age tables are ignored in this
                            option.
                            The default is false.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <!-- -  -  -  -  -  -  -  -  - transmission -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.transmission">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        When this element is set, it means that transmission output is generated. Attributes and child
                        elements may be used to configure the output on vector. &lt;br&gt;
                        Transmission is different, in this respect that it ignores any given age tables and the sex
                        attribute, as these are irrelevant for global transmission. Hence, there is no further
                        configuration
                        possible for this element.
                        //TODO RINKE #152846606-4: check the last sentence with Luc or Wilma.
                        &lt;p&gt;
                        It scores the following:
                        &lt;ul&gt;
                        &lt;li&gt;&lt;b&gt;mbr:&lt;/b&gt; monthly biting rate, mean number of bites per person with
                        exposure 1
                        &lt;li&gt;&lt;b&gt;mtp:&lt;/b&gt; monthly transmission potential = mbr times average number of
                        infective L3's per fly
                        &lt;li&gt;&lt;b&gt;L1:&lt;/b&gt; average number of L1's resulting from a single bite (x 1000)
                        &lt;li&gt;&lt;b&gt;L3:&lt;/b&gt; analogous for L3's
                        &lt;li&gt;&lt;b&gt;foi:&lt;/b&gt; force of infection, expected number of new worms per year in a
                        person with exposure 1
                        &lt;/ul&gt;
                        //TODO RINKE #152749391 ADAPT EXAMPLE.
                        &lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag.
                        &lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
                        &amp;lt;output ... &amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;vector vectorBites="true" toVectorFOI="true"&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;state name="susceptible" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;state name="alive" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/vector&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/elements&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        ... other items go here...&lt;br&gt;
                        &amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
    </xs:complexType>

    <!-- ########################### Offspring scores ##############################    -->
    <xs:complexType name="output.offspring.score.category.table">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        Defines offspring score number categories. Each category represents a range for the
                        number of offspring which a human scores (for example as a skin snip test, or as a real
                        count of offspring). For each range the number of humans that fall inside this score range
                        can be counted.
                        // TODO RINKE #152846606-3 : WE need an abstract parent here to arrange the lower levels, just
                        as with age.
                        //TODO RINKE #152749391 ADAPT EXAMPLE
                        &lt;p&gt;In the outputDef.xml file, this tag is placed inside the root output-tag.
                        &lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
                        &amp;lt;output ... &amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;age.table&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;age upper.age.limit="5" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;age upper.age.limit="20" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;age upper.age.limit="55" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;age upper.age.limit="95" /&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        &amp;lt;/age.table&amp;gt;&lt;br&gt;
                        &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        ... other elements go here...&lt;br&gt;
                        &amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element maxOccurs="unbounded" name="offspring.score.category" type="output.offspring.score.category">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="offspringScoreCategories"/>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    
    <xs:complexType name="output.offspring.score.category">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
                        Defines a single offspring score category for the output. This cateogory is limited by the
                        specified
                        upper limit, and the lower limit is the upper limit of the previous category (or 0 if no
                        previous
                        category was present).
                        // TODO RINKE #152846606-3 : WE need an abstract parent here to arrange the lower levels, just
                        as with age.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attribute name="upper.limit" use="required" type="mgz:positiveDouble"/>
    </xs:complexType>
    
</xs:schema>

example:

<elements>
    <demography sexes="false">
        <age.table>
            <age upper.age.limit="120"/>
        </age.table>
        <n.persons sexes="true">
            <age.table>
                <age upper.age.limit="10"/>
                <age upper.age.limit="25"/>
                <age upper.age.limit="125"/>
            </age.table>
        </n.persons>
        <!--<clinical.status refs="vi" sexes="true">
                <age.table>
                    <age upper.age.limit="17"/>
                </age.table>
            </clinical.status>  -->
        <worms sexes="true" wormTypes="prepatent patentFemale">
            <age.table>
                <age upper.age.limit="34"/>
                <age upper.age.limit="87"/>
            </age.table>
        </worms>
        <offspring sexes="true">
            <age.table>
                <age upper.age.limit="120"/>
            </age.table>
            <offspring.score.category.table>
                <offspring.score.category upper.limit="10"/>
                <offspring.score.category upper.limit="1000"/>
            </offspring.score.category.table>
        </offspring>
    </demography>
    
    <diagnostics>
        <age.table>
            <age upper.age.limit="14"/>
            <age upper.age.limit="66"/>
        </age.table>
        <diagnostics.table>
            <diagnostics ref="jdf"/>
            <diagnostics ref="slkdjf"/>
        </diagnostics.table>
        <offspring.score.category.table>
            <offspring.score.category upper.limit="10"/>
            <offspring.score.category upper.limit="500"/>
        </offspring.score.category.table>
    </diagnostics>
    
    <treatment sexes="false" distrTable="true">
        <age.table>
            <age upper.age.limit="3"/>
            <age upper.age.limit="99"/>
        </age.table>
    </treatment>
    
    <transmission/>
    
</elements>

