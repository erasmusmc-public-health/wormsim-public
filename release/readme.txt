Wormsim readme, updated for 4.7.1.
=================================

Documentation for this program can be found at the following locations:

1 - files in this directory:
    * readme.txt: this file, contains general information and how to start the program
    * changelog.txt: changes since previous versions 
    * input files in /input are example files
     
2 - the wiki for this project, at 
	https://gitlab.com/erasmusmc-public-health/wormsim.public/wikis/home. Specifically the *versions* document
	contains information on what is in this version (and what not). It can be found at
	https://gitlab.com/erasmusmc-public-health/wormsim.public/wikis/versions.
    
3 - Javadoc, at https://erasmgz.gitlab.io/wormsim/apidocs/. This contains code 
	comments, mostly interesting for programmers, but users may also find enough information here.
	(sorry, doesn't work at this time of writing, but will be fixed soon)

System requirements
===================

* at least 4GB on memory
* you must have java 17 installed. Beware that the program will NOT run on versions older than java 17. 
  It is not tested for java versions newer than java 17, but it will probably work with those.  


Running the program. 
====================

There is a shell script for mac users (run.sh/test.sh); they serve as an example which you can adapt at will.  

For windows, linux or mac you can run the program from the command line. Open a command prompt, 
and type in the following (the wormsim[version].jar should of course match the exact name of the jar file): 

java -jar wormsim[version].jar

This will give you an overview of command line options. 

Running the program with input: 

java -jar wormsim[version].jar -iinput/wormsim.xml -oinput/outputDef.xml -r1-1

Make sure the path indications for input files are correct; the path is relative for the directory in
which the program file is located. 
Also be aware that in the computer world, a file path with spaces in it will fail, unless you place it 
between "". This is because the computer will think that the file name ends when encountering the 
first space. So: -i"filename path with spaces.xml"

Output will be generated in the directory /output under the directory in which the program file 
is located.

Also, a batch file (run.bat) is present for windows users to easily run the program without having to 
type the long command line every time. An equivalent bash shell script is present for unix/mac/linux users.
A batch file or bash script is also available specifically for running wormsim from another program, like R. 
See below. 
 

If you need to run the program with a large number of agents: 
--------------------------------------------------------------
At present the program has been tested with half a million persons. That took 1.45 hours. More should be 
possible, but only after rewriting, so not with the present version. 
To run the person with a large number of persons, you need to tell java to reserve extra memory. 

java -Xmx7.5g -XX:-UseGCOverheadLimit -jar wormsim.jar -iinput/wormsim.xml -oinput/outputDef.xml -r0-1

The extra parameters for java tell jave to use extra memory, and to switch of safety measures about you're 
program taking a lot of time. 


WARNINGS
============
The program logs warnings to the console in case of any problems or strange situations
encountered. Please take these warnings serious. 

If you want to run wormsim from another program, like an R-script, there is a special 
runFromR.bat or runFromR.sh available, which calls the program without loosing the log warnings. 
With these, the log messages are saved to a file called "wormsimLog.txt", which can be 
found in the parent directory of the folder which contains the executable jar file of wormsim.


XML FILES
===============

The input files are all xml files. There is a general xml file and an outputDef.xml. 
XML is eXtended Markup Language, and these files can be validated before use. There
is absolutely no need to do this, but if you made a lot of changes in the structure, 
it might be sensible to do this. 
Validating goes against xsd schema's. These schema's contain the definitions to which
an xml file should be validated. How to do this is beyond the scope of this documentation;
there is plenty of information on the internet on this. 
 
The schema files are included in the /input/schema directory. Even if you don't use
them for validation, they can be used as a form of documentation, to look up what 
is allowed and what is not allowed in the input files. 

	